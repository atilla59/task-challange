<?php

namespace App\Http\Controllers;

use App\User;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser as JwtParser;

class AuthController extends Controller
{
    protected $server;
    protected $tokens;
    protected $jwt;

    public function __construct(AuthorizationServer $server,
                                TokenRepository $tokens,
                                JwtParser $jwt)
    {
        $this->jwt = $jwt;
        $this->server = $server;
        $this->tokens = $tokens;
    }

    public function login(ServerRequestInterface  $request)
    {


        try {

            $controller = new AccessTokenController($this->server, $this->tokens, $this->jwt);

            $request = $request->withParsedBody($request->getParsedBody() +
                [
                    'grant_type' => 'password',
                    'client_id' => 2, //client id
                    'client_secret' => "4leR7DqZccrKZGzPKvapBVvkWEZlhbU3WcWvTME2", //client secret
                ]);


            return with(new AccessTokenController($this->server, $this->tokens, $this->jwt))
                ->issueToken($request);

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            if ($e->getCode() === 400) {
                return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }

            return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        return response()->json('Logged out successfully', 200);
    }
}
