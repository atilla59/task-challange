<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::middleware('auth:api')->group(function () {

    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get("/appointments",function (Request $request){
        return $request->user()->appointments;
    });

    Route::post("/appointments",function (Request $request){
        return  $request->user()->appointments()->create([
            "name" =>$request->get("name"),
            "email" =>$request->get("email"),
            "phone" =>$request->get("phone"),
            "date" =>$request->get("date"),
            "to" =>"",
            "from" =>"Static"
        ]);
    });

    Route::put("/appointments/{id?}",function (Request $request,$id){
        $data  =   $request->user()->appointments->where("id",$id)->first()->update([
            "name" =>$request->get("name"),
            "email" =>$request->get("email"),
            "phone" =>$request->get("phone"),
            "date" =>$request->get("date"),
            "to" =>"",
            "from" =>"Static"
        ]);
        return $data ? $request->user()->appointments->where("id",$id)->first():"Error";
    });
    Route::get("/appointments/{id?}",function (Request $request ,$id=0){
        return $request->user()->appointments->where("id",$id)->first();
    });

    Route::post('/logout', 'AuthController@logout');


});

    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');
