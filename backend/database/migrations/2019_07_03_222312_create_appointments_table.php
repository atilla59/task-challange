<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("user_id")->nullable(false);
            $table->string("name",255)->default("John");
            $table->string("surname",255)->default("Doe");
            $table->string("email",30)->nullable("false");
            $table->string("phone",11)->nullable("false");
            $table->string("from",255)->nullable("false");
            $table->string("to",255)->nullable(false);
            $table->dateTime("date");
            $table->unsignedMediumInteger("arrival_time")->default(0);
            $table->timestamps();

            //Relations

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
