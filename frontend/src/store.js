/* eslint-disable consistent-return */
import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

axios.defaults.baseURL = 'http://localhost:8000/api';
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';


export default new Vuex.Store({
  state: {
    token: localStorage.getItem('access_token') || null,
    appointments: [],
    selectedAppointment: [],
    newAppointment:
      {
        name: 'aa',
        email: 'aa',
        phone: 'aa',
        date: new Date(),
      },
  },
  getters: {
    loggedIn(state) {
      return state.token !== null;
    },
    getAppointments(state) {
      return state.appointments;
    },
    getSelectedAppointment(state) {
      // eslint-disable-next-line no-param-reassign
      state.selectedAppointment.date = new Date(state.selectedAppointment.date);
      return state.selectedAppointment;
    },
    newAppointment(state) {
      // eslint-disable-next-line no-param-reassign
      return state.newAppointment;
    },
  },
  mutations: {
    retrieveToken(state, token) {
      // eslint-disable-next-line no-param-reassign
      state.token = token;
    },
    retrieveAppointments(state, appointments) {
      // eslint-disable-next-line no-param-reassign
      state.appointments = appointments;
    },
    appointmentById(state, appointment) {
      // eslint-disable-next-line no-param-reassign
      state.selectedAppointment = appointment;
    },
    newAppointment(state) {
      // eslint-disable-next-line no-param-reassign
      state.selectedAppointment = state.newAppointment;
    },
    clearAppointments(state) {
      // eslint-disable-next-line no-param-reassign
      state.appointments = [];
    },
    destroyToken(state) {
      // eslint-disable-next-line no-param-reassign
      state.token = null;
    },
  },
  actions: {
    clearAppointments(context) {
      context.commit('clearAppointments');
    },
    retrieveToken(context, credentials) {
      return new Promise((resolve, reject) => {
        axios.post('/login', {
          username: credentials.username,
          password: credentials.password,
        })
          .then((response) => {
            const token = response.data.access_token;

            localStorage.setItem('access_token', token);
            context.commit('retrieveToken', token);
            resolve(response);
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
    retrieveName(context) {
      axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;
      return new Promise((resolve, reject) => {
        axios.get('/user')
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    retrieveAppointments(context) {
      axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;
      axios.get('/appointments')
        .then((response) => {
          context.commit('retrieveAppointments', response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    appointmentById(context, id) {
      axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;
      axios.get(`/appointments/${id}`)
        .then((response) => {
          console.log(response.data);
          context.commit('appointmentById', response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    newAppointment(context) {
      context.commit('newAppointment');
    },
    destroyToken(context) {
      axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;
      if (context.getters.loggedIn) {
        return new Promise((resolve, reject) => {
          axios.post('/logout')
            .then((response) => {
              localStorage.removeItem('access_token');
              context.commit('destroyToken');
              resolve(response);
            })
            .catch((error) => {
              localStorage.removeItem('access_token');
              context.commit('destroyToken');
              reject(error);
            });
        });
      }
    },
    saveAppointment(context, content) {
      axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;
      // eslint-disable-next-line vars-on-top
      const date = new Date(content.data.date);
      // eslint-disable-next-line vars-on-top
      // eslint-disable-next-line camelcase
      const formatted_date = (`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`);
      return new Promise((resolve, reject) => {
        axios.post('/appointments', {
          name: content.data.name,
          email: content.data.email,
          phone: content.data.phone,
          date: formatted_date,

        })
          .then((response) => {
            context.commit('appointmentByID', response.data);
            resolve(response);
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
    updateAppointment(context, content) {
      axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;
      // eslint-disable-next-line vars-on-top
      const date = new Date(content.data.date);
      // eslint-disable-next-line vars-on-top
      // eslint-disable-next-line camelcase
      const formatted_date = (`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`);
      return new Promise((resolve, reject) => {
        axios.put(`/appointments/${content.data.id}`, {
          name: content.data.name,
          email: content.data.email,
          phone: content.data.phone,
          date: formatted_date,
        })
          .then((response) => {
            context.commit('appointmentByID', response.data);
            resolve(response);
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
  },
});
