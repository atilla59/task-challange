# task-challange

Coding challange 

# Frontend

$ npm install
$ npm run serve

for build $ npm run build

- Vue
- Axios
- Buefy UI Components (Bulma css) https://buefy.org/
- vue2-google-maps
- Vuex
- vue-router
- Dev Dep. (Eslint , Babel)

# Backend 

- Laravel 5.8
- Passport for Auth
- Mysql


# API Docs.
- Postman
- https://documenter.getpostman.com/view/3360430/SVSDQrg8?version=latest